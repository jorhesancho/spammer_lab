import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class MessageTemplate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @Column()
  subject: string;

  @Column()
  message: string;
}
