import { Repository } from "typeorm";
import { MessageTemplate } from "./message-template.entity";
import { messageTemplateRepo } from "./message-template.repo";

export class MessageTemplateService {
  constructor(private readonly repo: Repository<MessageTemplate>) {}

  public async getAll() {
    return this.repo.find();
  }
}

export const messageTemplateService = new MessageTemplateService(
  messageTemplateRepo
);
