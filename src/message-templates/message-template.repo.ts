import { appDataSource } from "../app-data-source";
import { MessageTemplate } from "./message-template.entity";

export const messageTemplateRepo =
  appDataSource.getRepository<MessageTemplate>(MessageTemplate);
