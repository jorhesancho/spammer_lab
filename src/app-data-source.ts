import { DataSource } from "typeorm";
import { Address } from "./address/address.entity";
import { MessageTemplate } from "./message-templates/message-template.entity";
// import { Book } from "./book/book.entity";

export const appDataSource = new DataSource({
  type: "mysql",
  host: process.env.DB_HOST,
  port: +(process.env.DB_PORT || ""),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_PASSWORD,
  entities: [Address, MessageTemplate],
  charset: "utf8mb4",
  // logging: true,
  // synchronize: true,
});
