require("dotenv").config();
import { appDataSource } from "../../app-data-source";
import { MessageTemplate } from "../../message-templates/message-template.entity";
import { mailTemplates } from "./message-templates.seed";

export const up = async () => {
  await appDataSource.initialize();
  await appDataSource
    .getRepository<MessageTemplate>(MessageTemplate)
    .save(mailTemplates);
  await appDataSource.destroy();
  console.log(`Migration seed mail templates up`);
};

up();
