require("dotenv").config();
import { appDataSource } from "../../app-data-source";
import { MessageTemplate } from "../../message-templates/message-template.entity";

export const down = async () => {
  await appDataSource.initialize();
  await appDataSource
    .getRepository<MessageTemplate>(MessageTemplate)
    .delete({});
  await appDataSource.destroy();
  console.log(`Migration seed mail templates down`);
};

down();
