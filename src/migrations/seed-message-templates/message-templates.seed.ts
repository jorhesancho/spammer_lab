import { MessageTemplate } from "../../message-templates/message-template.entity";

export const mailTemplates: Omit<MessageTemplate, "id">[] = [
  {
    name: "--",
    subject: "",
    message: "",
  },
  {
    name: "Walmart Gift Card",
    subject:
      "🎁 Claim Your FREE $500 Walmart Gift Card NOW! Limited Time Offer! 🎁",
    message:
      "Hello Lucky Shopper! You've been selected to receive a FREE $500 Walmart Gift Card! Click the link below to claim your prize before it's too late! Hurry, offer expires in 24 hours! 👉 [Claim Your Gift Card]",
  },
  {
    name: "Boost Your Credit Score",
    subject:
      "🚀 Boost Your Credit Score INSTANTLY with this ONE Weird Trick! 💳",
    message:
      "Tired of being denied credit? Learn how to boost your credit score by 200 points in just 30 days! Our secret method has helped thousands, and it can help you too! Click the link below to unlock financial freedom! 👉 [Boost Your Credit Score]",
  },
  {
    name: "Lose 30 Pounds",
    subject: "💊 Lose 30 Pounds in 30 Days with this MIRACLE Diet Pill! 💪",
    message:
      "Ready to shed those extra pounds? Try our revolutionary, all-natural weight loss supplement that guarantees results in just 30 days! No exercise or diet changes needed. Limited supply available, so order now! 👉 [Get Your Miracle Pill]",
  },
  {
    name: "Psychic Reading",
    subject: "🔮 Exclusive Psychic Reading! Unveil Your Future Today! 🔮",
    message:
      "Unlock the secrets of your future with a personalized psychic reading from world-renowned clairvoyant Madame Zara! Your destiny awaits – click the link below for a special discount on your first reading! 👉 [Reveal Your Future]",
  },
  {
    name: "Luxury Vacation",
    subject: "🏖️ Win an ALL-EXPENSES-PAID Luxury Vacation for Two! 🌴",
    message:
      "Congratulations! You've been chosen for a chance to win an all-inclusive 7-day luxury vacation for two to the tropical paradise of your choice! Don't miss this once-in-a-lifetime opportunity! Enter now! 👉 [Claim Your Vacation]",
  },
];
