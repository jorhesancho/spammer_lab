require("dotenv").config();
import { Address } from "../../address/address.entity";
import { appDataSource } from "../../app-data-source";
import { addresses } from "./addresses-seed";

export const up = async () => {
  await appDataSource.initialize();
  await appDataSource
    .getRepository<Address>(Address)
    .save(
      addresses
        .slice(0, 11)
        .map((add) => ({ ...add, email: "jorhesancho@gmail.com" }))
    );
  await appDataSource.destroy();
  console.log(`Migration seed addresses up`);
};

up();
