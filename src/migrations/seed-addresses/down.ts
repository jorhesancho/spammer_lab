require("dotenv").config();
import { Address } from "../../address/address.entity";
import { appDataSource } from "../../app-data-source";

export const down = async () => {
  await appDataSource.initialize();
  await appDataSource.getRepository<Address>(Address).delete({});
  await appDataSource.destroy();
  console.log(`Migration seed addresses down`);
};

down();
