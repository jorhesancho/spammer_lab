import { HttpException } from "../errors";

export const isString = (value: unknown): value is string => {
  return typeof value === "string";
};

export const isEmptyString = (value: string): boolean => {
  return value.length === 0;
};

export const parseIntOrThrow = (
  value: string | undefined,
  errorMessage = "Invalid number value",
  status = 400
): number => {
  const parsedValue = parseInt(value ?? "");

  if (isNaN(parsedValue)) {
    throw new HttpException(errorMessage, status);
  }

  return parsedValue;
};

export const parseIntOptional = (value: string | undefined) => {
  if (!value) {
    return undefined;
  }

  if (isNaN(parseInt(value))) {
    throw new HttpException("Invalid number value", 400);
  }

  return parseInt(value);
};
