export const isDupEntryError = (err: unknown): boolean => {
  return !!err && (err as { code: string }).code === "ER_DUP_ENTRY";
};
