import { HttpException } from "../http-exception";

export const filterError = (
  predicate: (err: unknown) => boolean,
  errorFactory: () => Error
) => {
  return (err: unknown): never => {
    if (predicate(err)) {
      throw errorFactory();
    }

    throw err;
  };
};

export const matchError = (
  predicate: (err: unknown) => boolean,
  message: string,
  status = 500
) => {
  return (err: unknown): never => {
    if (predicate(err)) {
      throw new HttpException(message, status);
    }

    throw err;
  };
};
