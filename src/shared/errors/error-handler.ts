import { ErrorRequestHandler } from "express";

export const errorHandler =
  (): ErrorRequestHandler => (err, req, res, next) => {
    console.error(err);

    res.json({
      message: err.message,
      status: err.status ?? 500,
    });
  };
