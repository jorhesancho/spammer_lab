import { httpClient, formDataToObject } from "./http-client.js";

export const handleEdit = async (event) => {
  event.preventDefault();

  const data = formDataToObject(new FormData(event.target));
  const id = event.target.getAttribute("data-address-id");

  await httpClient.put(`/addresses/${id}`, data).catch((error) => {
    console.error("Error submitting the form:", error);
  });

  location.reload();
};

export const handleDelete = async (event) => {
  event.preventDefault();

  const id = event.target.getAttribute("data-address-id");

  await httpClient.delete(`/addresses/${id}`).catch((error) => {
    console.error("Error submitting the form:", error);
  });

  location.reload();
};
