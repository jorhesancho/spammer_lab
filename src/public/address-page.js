import { handleEdit, handleDelete } from "./address-actions.js";
import { handleMessageTemplateSelect } from "./message-templates.js";

document.querySelectorAll(".edit-address-form").forEach((form) => {
  form.addEventListener("submit", handleEdit);
});

document.querySelectorAll(".delete-address-form").forEach((form) => {
  form.addEventListener("submit", handleDelete);
});

document
  .getElementById("messageTemplateSelect")
  .addEventListener("change", handleMessageTemplateSelect);
