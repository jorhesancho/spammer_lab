export const handleMessageTemplateSelect = (event) => {
  const selectElement = event.target;
  const option = selectElement.options[selectElement.selectedIndex];

  const subject = option.getAttribute("data-subject") ?? "",
    message = option.getAttribute("data-message") ?? "";

  document.getElementById("subject-input").value = subject;
  document.getElementById("message-textarea").value = message;
};
