import { appDataSource } from "../app-data-source";
import { Address } from "./address.entity";

export const addressRepo = appDataSource.getRepository<Address>(Address);
