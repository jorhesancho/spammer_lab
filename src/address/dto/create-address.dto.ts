import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class CreateAddressDto {
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  patronymic: string;
}
