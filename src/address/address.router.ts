import { Router } from "express";
import { addressController } from "./address.controller";

export const addressRouter = Router();

addressRouter.get("/", (req, res, next) => {
  return addressController.getAll(req, res).catch((e) => next(e));
});

addressRouter.get("/:id", (req, res, next) => {
  return addressController.getOne(req, res).catch((e) => next(e));
});

addressRouter.post("/", (req, res, next) => {
  return addressController.create(req, res).catch((e) => next(e));
});

addressRouter.post("/send-mail", (req, res, next) => {
  return addressController.sendMail(req, res).catch((e) => next(e));
});

addressRouter.put("/:id", (req, res, next) => {
  return addressController.update(req, res).catch((e) => next(e));
});

addressRouter.delete("/:id", (req, res, next) => {
  return addressController.delete(req, res).catch((e) => next(e));
});
