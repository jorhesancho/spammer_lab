import { Request, Response } from "express";
import { throwIfNoValue } from "../shared/utils/throw-if-no-value";
import { AddressService, addressService } from "./address.service";

import { parseIntOrThrow } from "../shared/utils/validation-utils";
import { HttpException } from "../shared";
import { EmailService, emailService } from "./email.service";
import {
  MessageTemplateService,
  messageTemplateService,
} from "../message-templates/message-template.service";

export class AddressController {
  constructor(
    private readonly addressService: AddressService,
    private readonly emailService: EmailService,
    private readonly messageTemplateService: MessageTemplateService
  ) {}

  public async create(req: Request, res: Response): Promise<void> {
    const dto = req.body;

    await this.addressService.save(dto);

    res.status(201).redirect("addresses");
  }

  public async getAll(req: Request, res: Response): Promise<void> {
    const page = parseInt(req.query.page as string) || 1;
    const size = parseInt(req.query.size as string) || 10;

    const { addresses, hasNext } = await this.addressService.getAll(page, size);

    const messageTemplates = await this.messageTemplateService.getAll();

    res.render("address-list-page", {
      addresses,
      hasNext,
      currentPage: page,
      messageTemplates,
    });
  }

  public async getOne(req: Request, res: Response): Promise<void> {
    const bookId = parseIntOrThrow(req.params.id, "Invalid book id");

    const book = await this.addressService
      .getById(bookId)
      .then(throwIfNoValue(() => new HttpException("Book not found", 404)));

    res.json(book);
  }

  public async update(req: Request, res: Response): Promise<void> {
    const dto = req.body;

    const id = parseIntOrThrow(req.params.id, "Invalid book id");

    await this.addressService.update(id, dto);

    res.status(204).send();
  }

  public async delete(req: Request, res: Response): Promise<void> {
    const id = parseIntOrThrow(req.params.id, "Invalid book id");

    await this.addressService.delete(id);

    res.status(204).send();
  }

  public async sendMail(req: Request, res: Response): Promise<void> {
    const { message, subject } = req.body;

    await this.emailService.sendToAllAddresses(message, subject);

    res.status(201).redirect("/addresses");
  }
}

export const addressController = new AddressController(
  addressService,
  emailService,
  messageTemplateService
);
