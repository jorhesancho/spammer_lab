import { Repository } from "typeorm";
import { Address } from "./address.entity";
import { addressRepo } from "./address.repository";

export class AddressService {
  constructor(private readonly repo: Repository<Address>) {}

  public async save(dto: Partial<Address>): Promise<Address> {
    return this.repo.save(this.repo.create(dto));
  }

  public async getAll(
    page: number,
    size: number
  ): Promise<{ addresses: Address[]; hasNext: boolean }> {
    const skip = (page - 1) * size;

    const items = await this.repo.find({
      skip,
      take: size + 1,
      order: {
        createdAt: "desc",
      },
    });

    return {
      addresses: items.slice(0, size),
      hasNext: items.length > size,
    };
  }

  public getById(id: number) {
    return this.repo.findOneBy({ id });
  }

  public async update(id: number, dto: Partial<Address>): Promise<void> {
    await this.repo.update(id, this.repo.create(dto));
  }

  public async delete(id: number) {
    await this.repo.delete(id);
  }
}

export const addressService = new AddressService(addressRepo);
