import { Repository } from "typeorm";
import nodemailer from "nodemailer";
import { Address } from "./address.entity";
import { addressRepo } from "./address.repository";

const createTransport = () => {
  return nodemailer.createTransport({
    host: process.env.MAIL_HOST ?? "",
    port: parseInt(process.env.MAIL_PORT ?? ""),
    secure: false,
    auth: {
      user: process.env.MAIL_USER ?? "",
      pass: process.env.MAIL_PASS ?? "",
    },
  });
};

const defaultTransporter = createTransport();

export class EmailService {
  constructor(private readonly repo: Repository<Address>) {}

  public async sendToAllAddresses(
    message: string,
    subject?: string
  ): Promise<void> {
    const addresses = await this.repo.find();

    const transporter = defaultTransporter || createTransport();

    for (const address of addresses) {
      const mailOptions = {
        from: "Spammer <jorhesancho@gmail.com>",
        to: address.email,
        subject: subject || "Spam",
        text: message,
      };

      await transporter.sendMail(mailOptions).catch((e) => {
        console.error(
          `Could not send email to address ${address.email}: ${e.message}`
        );
      });
    }
  }
}

export const emailService = new EmailService(addressRepo);
