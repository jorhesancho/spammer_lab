import express from "express";
import * as dotenv from "dotenv";
dotenv.config();

import { appDataSource } from "./app-data-source";
import { errorHandler } from "./shared/errors";
import * as path from "path";
import { addressRouter } from "./address/address.router";

appDataSource
  .initialize()
  .then(() => {
    console.log("Data Source has been initialized!");
  })
  .catch((err) => {
    console.error("Error during Data Source initialization:", err);
  });

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "/public")));

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "/views"));

app.get("/", (req, res) => {
  res.redirect("/addresses");
});

app.use("/addresses", addressRouter);

app.use(errorHandler());

const PORT = process.env.APP_PORT ?? 3000;

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
